import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { CoinComponent } from "./coin/coin.component";
import { ButtonModule } from "primeng/button";
import { ImageModule } from "primeng/image";
import { PanelModule } from "primeng/panel";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HexLineComponent } from './hex-line/hex-line.component';

@NgModule({
  declarations: [AppComponent, CoinComponent, HexLineComponent],
  imports: [
    BrowserModule,
    ButtonModule,
    ImageModule,
    PanelModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
