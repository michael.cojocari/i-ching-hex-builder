export interface HexLine {
    [key: number]: string;
}
