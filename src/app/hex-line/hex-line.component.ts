import {Component, Input} from '@angular/core';
import {HexLine} from "./hex-line-image.interface";

@Component({
  selector: 'hex-line',
  templateUrl: './hex-line.component.html',
  styleUrls: ['./hex-line.component.css']
})
export class HexLineComponent {
  @Input() hexValue:number = 0;
  hexLines: HexLine = {
    9 : "assets/old-yang.jpg",
    6 : "assets/old-yin.jpg",
    7 : "assets/young-yang.jpg",
    8 : "assets/young-yin.jpg"
  }

}
