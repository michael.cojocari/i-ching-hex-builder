import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HexLineComponent } from './hex-line.component';

describe('HexLineComponent', () => {
  let component: HexLineComponent;
  let fixture: ComponentFixture<HexLineComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HexLineComponent]
    });
    fixture = TestBed.createComponent(HexLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
