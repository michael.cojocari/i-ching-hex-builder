import { Component } from "@angular/core";
import {Hex, YANG_VALUE, YIN_VALUE} from "./hex.class";
import {hexData} from "./hex-data";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "i-ching-hex-builder";
  coins = [3, 3, 3];
  currentHex: Hex = new Hex();
  currentHexLine: number = 0;
  currentHexSums: number[] = [0,0,0,0,0,0];

  constructor() {
    console.log(hexData);
  }

  coinClick(coinIndex: number, newCoinValue: number) {
    this.coins[coinIndex] = newCoinValue;
  }

  setHexLine() {
    const coinSum = this.coins[0] + this.coins[1] + this.coins[2];
    this.currentHexSums[this.currentHexLine] = coinSum;
    let hexValue: number = 0;
    if (coinSum === 9 || coinSum === 7) {
      hexValue = YANG_VALUE;
    } else {
      hexValue = YIN_VALUE;
    }

    switch (this.currentHexLine) {
      case 0: {
        this.currentHex.line1 = hexValue;
        break;
      }
      case 1: {
        this.currentHex.line2 = hexValue;
        break;
      }
      case 2: {
        this.currentHex.line3 = hexValue;
        break;
      }
      case 3: {
        this.currentHex.line4 = hexValue;
        break;
      }
      case 4: {
        this.currentHex.line5 = hexValue;
        break;
      }
      case 5: {
        this.currentHex.line6 = hexValue;
        break;
      }
    }
    this.currentHexLine++;
    this.coins = [3, 3, 3];
    console.log(this.currentHex);
  }

  isHexDefined() {
    return (this.currentHexLine > 5);
  }

  clearHex() {
    this.currentHex = new Hex();
    this.currentHexSums = [0,0,0,0,0,0];
    this.coins=[3,3,3];
    this.currentHexLine = 0;
  }
}
