import { Hex, YANG_VALUE, YIN_VALUE } from './hex.class';
import { HexDatum } from './hex-datum.class';

export let hexData: HexDatum[] = [];

hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE
    ),
    1
  ),
];

hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE),
    2
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YANG_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YANG_VALUE),
    3
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YANG_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YANG_VALUE, YIN_VALUE),
    4
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YIN_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE
    ),
    5
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YIN_VALUE
    ),
    6
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YANG_VALUE, YIN_VALUE),
    7
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YANG_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE),
    8
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE
    ),
    9
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE
    ),
    10
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YIN_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE
    ),
    11
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YIN_VALUE
    ),
    12
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE
    ),
    13
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE
    ),
    14
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YIN_VALUE, YIN_VALUE, YANG_VALUE, YIN_VALUE, YIN_VALUE),
    15
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YIN_VALUE, YANG_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE),
    16
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YANG_VALUE
    ),
    17
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE
    ),
    18
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YANG_VALUE, YANG_VALUE),
    19
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YANG_VALUE, YANG_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE),
    20
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YANG_VALUE
    ),
    21
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YANG_VALUE
    ),
    22
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YANG_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE),
    23
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YANG_VALUE),
    24
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YANG_VALUE
    ),
    25
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YANG_VALUE,
      YIN_VALUE,
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE
    ),
    26
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(YANG_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YIN_VALUE, YANG_VALUE),
    27
  ),
];
hexData = [
  ...hexData,
  new HexDatum(
    new Hex(
      YIN_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YANG_VALUE,
      YIN_VALUE
    ),
    28
  ),
];
