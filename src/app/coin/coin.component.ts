import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'coin',
  templateUrl: './coin.component.html',
  styleUrls: ['./coin.component.css']
})
export class CoinComponent {

  @Input() coinValue= 0;
  @Output() coinClick: EventEmitter<number> = new EventEmitter<number>();

  coinClicked() {
    if (this.coinValue === 3) {
      this.coinClick.emit(2);
    } else {
      this.coinClick.emit(3);
    }
  }
}
