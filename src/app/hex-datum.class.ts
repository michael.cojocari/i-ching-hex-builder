import { Hex } from "./hex.class";


/**
 * 1 - Yang
 * 2 - Yin
 */
export class HexDatum {
    hex: Hex;
    hexNumber: number;
  
    constructor(hex: Hex = new Hex(), hexNumber: number = 0) {
      this.hex = hex;
      this.hexNumber = hexNumber;
    }
  }