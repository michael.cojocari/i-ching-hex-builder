/**
 * 1 - YANG
 * 2 - YIN
 */

export const YANG_VALUE = 1;
export const YIN_VALUE = 2;

export class Hex {
  line6: number;
  line5: number;
  line4: number;
  line3: number;
  line2: number;
  line1: number;

  constructor(
    line6: number = 0,
    line5: number = 0,
    line4: number = 0,
    line3: number = 0,
    line2: number = 0,
    line1: number = 0
  ) {
      this.line1 = line1;
      this.line2 = line2;
      this.line3 = line3;
      this.line4 = line4;
      this.line5 = line5;
      this.line6 = line6;
  }
}
